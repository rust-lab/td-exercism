// double every 2 digits, starting from right. If > 9, remove 9
// 0 1 2 3 4 5 6 7 8 9
// 0 2 4 6 8 1 3 5 7 9

// sum digits, valid luhn checksum <=> sum % 10 == 0
// 1-length strings are not valid
// characters allowed : [0-9] and space

pub enum Error {
    InvalidChar(char),
}

/// Check a Luhn checksum.
pub fn is_valid(code: &str) -> bool {
    match trim(code) {
        Ok(i) => {
            if i.len() < 2 {
                false
            } else {
                validate(&i)
            }
        }
        Err(_) => false,
    }
}

fn trim(code: &str) -> Result<Vec<u32>, Error> {
    let mut v = Vec::new();
    for c in code.chars().filter(|c| c != &' ') {
        match c.to_digit(10) {
            None => return Err(Error::InvalidChar(c)),
            Some(u) => v.insert(0, u),
        }
    }
    Ok(v)
    // https://exercism.io/tracks/rust/exercises/luhn/solutions/b310e5203500486398d482b47561cac1
    // try_fold, map_or..
}

fn validate(input: &[u32]) -> bool {
    input
        .iter()
        .enumerate()
        .fold(0, |a, (i, d)| a + if i % 2 == 1 { convert(*d) } else { *d })
        % 10
        == 0
}

fn convert(n: u32) -> u32 {
    let d = n * 2;
    if d > 9 {
        d - 9
    } else {
        d
    }
}
