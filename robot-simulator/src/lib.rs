// The code below is a stub. Just enough to satisfy the compiler.
// In order to pass the tests you can add-to or change any of this code.

#[derive(PartialEq, Debug)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

#[derive(Debug)]
pub struct Robot {
    x: i32,
    y: i32,
    dir: Direction,
}

impl Robot {
    pub fn new(x: i32, y: i32, d: Direction) -> Self {
        Robot { x, y, dir: d }
    }

    pub fn turn_right(mut self) -> Self {
        match self.dir {
            Direction::North => self.dir = Direction::East,
            Direction::East => self.dir = Direction::South,
            Direction::South => self.dir = Direction::West,
            Direction::West => self.dir = Direction::North,
        };
        self
    }

    pub fn turn_left(mut self) -> Self {
        match self.dir {
            Direction::North => self.dir = Direction::West,
            Direction::East => self.dir = Direction::North,
            Direction::South => self.dir = Direction::East,
            Direction::West => self.dir = Direction::South,
        };
        self
    }

    pub fn advance(mut self) -> Self {
        match self.dir {
            Direction::North => self.y += 1,
            Direction::East => self.x += 1,
            Direction::South => self.y -= 1,
            Direction::West => self.x -= 1,
        };
        self
    }

    pub fn instructions(mut self, instructions: &str) -> Self {
        for cmd in instructions.chars() {
            self = match cmd {
                'A' => self.advance(),
                'L' => self.turn_left(),
                'R' => self.turn_right(),
                _ => {
                    println!("unknown command : {}", cmd);
                    self
                }
            };
        }
        self
    }

    pub fn position(&self) -> (i32, i32) {
        (self.x, self.y)
    }

    pub fn direction(&self) -> &Direction {
        &self.dir
    }
}
