use core::convert::TryFrom;

#[derive(Debug, PartialEq)]
pub enum Error {
    IncompleteNumber,
    Overflow,
}

/// Convert a list of numbers to a stream of bytes encoded with variable length encoding.
pub fn to_bytes(values: &[u32]) -> Vec<u8> {
    let mut vec = Vec::new();
    for num in values {
        vec.extend(Vlq::from(*num).data);
    }
    vec
}

/// Given a stream of bytes, extract all numbers which are encoded in there.
pub fn from_bytes(bytes: &[u8]) -> Result<Vec<u32>, Error> {
    let mut result = Vec::new();
    let mut vec = Vec::new();
    for b in bytes {
        vec.push(*b);
        if b & 0x80 == 0x00 {
            //last byte, compute :
            match u32::try_from(Vlq::new(vec.as_slice())) {
                Ok(num) => result.push(num),
                Err(err) => {
                    return Err(err);
                }
            }

            vec = Vec::new();
        }
    }
    if vec.is_empty() {
        Ok(result)
    } else {
        Err(Error::IncompleteNumber)
    }
}

#[derive(Debug, PartialEq)]
pub struct Vlq {
    data: Vec<u8>,
}

impl Vlq {
    pub fn new(byte_array: &[u8]) -> Vlq {
        Vlq {
            data: Vec::from(byte_array),
        }
    }
}

impl From<u32> for Vlq {
    fn from(mut num: u32) -> Self {
        let mut vec: Vec<u8> = Vec::new();
        let mut first = true;
        loop {
            let digit: u8 = (num % 128) as u8;
            num /= 128;
            if first {
                vec.insert(0, digit);
                first = false;
            } else {
                vec.insert(0, digit | 128);
            }
            if num == 0 {
                break;
            }
        }

        Vlq::new(vec.as_ref())
    }
}

impl TryFrom<Vlq> for u32 {
    type Error = Error;
    fn try_from(mut vlq_in: Vlq) -> std::result::Result<Self, Self::Error> {
        let mut res: u32 = 0;
        println!("try_from {:?}", vlq_in.data);
        loop {
            if vlq_in.data.is_empty() {
                // something was expected here
                return Err(Error::IncompleteNumber);
            }
            let d = vlq_in.data.remove(0);
            res += (d & 127_u8) as u32;

            if d & 128_u8 == 0_u8 {
                // no more digits
                break;
            } else {
                // next digit to come
                
                match res.checked_mul(128) {
                    Some(v) => res = v,
                    None => return Err(Error::Overflow),
                } 
                
                
            }
        }
        if vlq_in.data.is_empty() {
            Ok(res)
        } else {
            println!("too much {:?}", vlq_in.data);
            // too much digits
            Err(Error::Overflow)
        }
    }
}
