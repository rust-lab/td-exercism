use std::collections::HashMap;
use std::sync::mpsc;
use std::thread;

pub fn frequency(input: &[&str], worker_count: usize) -> HashMap<char, usize> {

    let line: String = input.join("").to_string();

    let (tx, rx) = mpsc::channel();

    // split line for workers to work on :
    let chunk_size = line.len()/worker_count;

    for i in 0..worker_count-1 {
        let tx_i = tx.clone();
        let str_i = (line[i*chunk_size .. (i+1)*chunk_size]).to_string();

        thread::spawn( move || {
                //println!("Thread {}, count : {}", i, str_i.len());
                let mut res = single_frequency(str_i);
                for (k,v) in res.drain() {
                    tx_i.send((k,v)).unwrap();
                }
            });
    }
    // last chunk
    let str_last = line[(worker_count-1)*chunk_size..].to_string();
    thread::spawn( move || {
                    //println!("last Thread, count : {}", str_last.len());
                    let mut res = single_frequency(str_last);
                    for (k,v) in res.drain() {
                        tx.send((k,v)).unwrap();
                    }
                });






    let mut hasmap_res = HashMap::new();
    for (k,v) in rx {
        match hasmap_res.get_mut(&k) {
            Some(old) => *old += v,
            None => {hasmap_res.insert(k,v);},
        }

    }
    hasmap_res




}

/// Taking the solution in text
/// Simple sequential char frequency. Can it be beat?
pub fn single_frequency(line: String) -> HashMap<char, usize> {
    let mut map = HashMap::new();

    for chr in line.chars().filter(|c| c.is_alphabetic()) {
        if let Some(c) = chr.to_lowercase().next() {
            (*map.entry(c).or_insert(0)) += 1;
        }
    }
    map
}