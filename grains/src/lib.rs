pub fn square(s: u32) -> u64 {
    assert!(s>0 && s<65, "Square must be between 1 and 64");
    2u64.pow(s-1)
    // the left-shift is nice too : 1u64.wrapping_shl(s-1)
}

pub fn total() -> u64 {
    (1..65).map(|s| square(s)).sum()    
}


