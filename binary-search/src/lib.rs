use std::fmt::{Debug, Display};

pub fn find<T: Ord + Display + Debug, U: AsRef<[T]> + Debug>(array: U, key: T) -> Option<usize> {
    find_rec(array.as_ref(), key, 0)
}

fn find_rec<T: Ord + Display + Debug>(array: &[T], key: T, offset: usize) -> Option<usize> {
    // we consider the array sorted.

    println!("array {:?}, key: {}", array, key);

    // end the recursion
    if array.is_empty() {
        return None;
    }

    let pivot = array.len() / 2;

    println!("pivot {}", pivot);

    if array[pivot] == key {
        println!("=");
        return Some(pivot + offset);
    }
    if array[pivot] > key {
        println!(">");
        return find_rec(&array[..pivot], key, offset);
    }
    if array[pivot] < key {
        println!("<");
        return find_rec(&array[pivot + 1..], key, pivot + offset + 1);
    }
    // interresting way of handling the offset : 
    // https://exercism.io/tracks/rust/exercises/binary-search/solutions/87664ac5148c47389043c9f055aa7560
    unreachable!(" ? ");
}
// and a iterative version : https://exercism.io/tracks/rust/exercises/binary-search/solutions/c55e5974c95040c5a069d6bcedec3ee9