/**
 * An Armstrong number is a number that is the sum of its own digits each raised to the power of the number of digits.

    9 is an Armstrong number, because 9 = 9^1 = 9
    10 is not an Armstrong number, because 10 != 1^2 + 0^2 = 1
    153 is an Armstrong number, because: 153 = 1^3 + 5^3 + 3^3 = 1 + 125 + 27 = 153
    154 is not an Armstrong number, because: 154 != 1^3 + 5^3 + 4^3 = 1 + 125 + 64 = 190
 

    nice solution : https://exercism.io/tracks/rust/exercises/armstrong-numbers/solutions/79d4f3931f6047de9d9642b57f7c5337
 
 */

fn digits(mut num:  u32) -> Vec<u32> {
    let mut digits=Vec::new();
    if num == 0 {
        digits.push(0);
    }

    while num != 0 {
        let q= num/10;
        let r= num%10;
        digits.push(r);
        num = q;
    } 
    digits
}


pub fn is_armstrong_number(num: u32) -> bool {
    let d = digits(num); // num copied to call fun, and mutate in it
    let len:u32 = d.len() as u32;
    d.iter().map(|i| i.pow(len)).sum::<u32>() == num

}

#[test]
pub fn digits_0() {
    assert_eq!(vec![0], digits(0));
}

#[test]
pub fn digits_14() {
    assert_eq!(vec![4,1], digits(14));
}

#[test]
pub fn digits_256() {
    assert_eq!(vec![6,5,2], digits(256));
}