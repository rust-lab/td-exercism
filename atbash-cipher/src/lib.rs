use affinecipher as ac;

// lazy impl as I already did Affine cipher:
// this cipher is just an affine cipher, with coeff = -1 (or 25) and offset = -1
// see https://exercism.io/tracks/rust/exercises/affine-cipher/solutions/71c78e67bdda437880efc641ea3f2de3

/// "Encipher" with the Atbash cipher.
pub fn encode(plain: &str) -> String {
   ac::encode(plain, 25, -1).unwrap()
}

/// "Decipher" with the Atbash cipher.
pub fn decode(cipher: &str) -> String {
    ac::decode(cipher, 25, -1).unwrap()
}
