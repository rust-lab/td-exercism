
// TODO : make the trait derivable
// see https://exercism.io/tracks/rust/exercises/dot-dsl/solutions/caa9a67e77294429bfdd430eb8d7d052
pub trait Attributes {
    fn with_attrs(self, attrs: &[(&str, &str)]) -> Self;
    fn get_attr(&self, key: &str) -> Option<&str>;
}

