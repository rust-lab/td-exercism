use crate::traits::Attributes;
use std::collections::HashMap;
use macro_derive::Attributes;

#[derive(Debug, PartialEq, Clone, Attributes)]
pub struct Edge {
    from: String,
    to: String,
    attrs: HashMap<String, String>,
}

impl Edge {
    pub fn new(n1: &str, n2: &str) -> Self {
        Edge {
            from: n1.to_string(),
            to: n2.to_string(),
            attrs: HashMap::default(),
        }
    }
}
/*
impl Attributes for Edge {
    fn with_attrs(mut self, attrs: &[(&str, &str)]) -> Self {
        self.attrs = HashMap::default();
        for (k, v) in attrs {
            self.attrs.insert(k.to_string(), v.to_string());
        }
        self
    }

    fn get_attr(&self, key: &str) -> Option<&str> {
        unimplemented!()
    }
}
*/