use crate::traits::Attributes;
use maplit::hashmap;
use std::collections::HashMap;

#[derive(Debug, PartialEq, Clone)]
pub struct Node {
    value: String,
    attrs: HashMap<String, String>,
}

impl Node {
    pub fn new(val: &str) -> Self {
        Node {
            value: val.to_string(),
            attrs: HashMap::default(),
        }
    }

    pub fn has_value(&self, v: &str) -> bool {
        self.value == v
    }
}

impl Attributes for Node {
    fn with_attrs(mut self, attrs: &[(&str, &str)]) -> Self {
        self.attrs = HashMap::default();
        for (k, v) in attrs {
            self.attrs.insert(k.to_string(), v.to_string());
        }
        self
    }

    fn get_attr(&self, key: &str) -> Option<&str> {
        self.attrs
            .iter()
            .filter(|(k, v)| *k == key)
            .map(|(k, v)| v)
            .nth(0)
            .map(|s| s.as_str())
    }
}
