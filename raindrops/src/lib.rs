fn div_by(n: u32, q: u32) -> bool {
    n % q == 0
}

pub fn raindrops(n: u32) -> String {
    match (div_by(n, 3), div_by(n, 5), div_by(n, 7)) {
        (false, false, false) => return format!("{}", n),
        (three, five, seven) => {
            let mut s: String = String::new();
            if three {
                s = s + "Pling";
            }
            if five {
                s = s + "Plang";
            }
            if seven {
                s = s + "Plong";
            }
            return s;
        }
    }
}
