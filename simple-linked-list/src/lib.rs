use std::iter::FromIterator;
use std::ops::Deref;

pub struct SimpleLinkedList<T> {
    head: Option<Box<Node<T>>>,
}

struct Node<T> {
    data: T,
    next: Option<Box<Node<T>>>,
}

impl<T> Node<T> {
    fn new(elem: T) -> Self {
        Node {
            data: elem,
            next: None,
        }
    }

    fn len(&self) -> usize {
        1 + match &self.next {
            None => 0,
            Some(next) => next.deref().len(),
        }
    }
}

impl<T> SimpleLinkedList<T> {
    pub fn new() -> Self {
        SimpleLinkedList { head: None }
    }

    pub fn len(&self) -> usize {
        match &self.head {
            None => 0,
            Some(node) => node.deref().len(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.head.is_none()
    }

    pub fn push(&mut self, element: T) {
        let mut node = Node::new(element);
        match self.head.take() {
            None => self.head = Some(Box::new(node)),
            Some(old_head) => {
                node.next = Some(old_head);
                self.head = Some(Box::new(node));
            }
        }
    }

    pub fn pop(&mut self) -> Option<T> {
        self.head.take().map(|boxed| {
            self.head = boxed.next;
            boxed.data
        })
    }

    pub fn peek(&self) -> Option<&T> {
        self.head.as_ref().map(|boxed| &boxed.data)
    }

    // consumes the list
    pub fn rev(mut self) -> SimpleLinkedList<T> {
        let mut list = SimpleLinkedList::new();
        while let Some(h) = self.pop() {
            list.push(h);
        }
        list
    }
}

impl<T> Default for SimpleLinkedList<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> FromIterator<T> for SimpleLinkedList<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let mut list = SimpleLinkedList::new();
        for i in iter {
            list.push(i);
        }
        list
    }
}

// In general, it would be preferable to implement IntoIterator for SimpleLinkedList<T>
// instead of implementing an explicit conversion to a vector. This is because, together,
// FromIterator and IntoIterator enable conversion between arbitrary collections.
// Given that implementation, converting to a vector is trivial:
//
// let vec: Vec<_> = simple_linked_list.into_iter().collect();
//
// The reason this exercise's API includes an explicit conversion to Vec<T> instead
// of IntoIterator is that implementing that interface is fairly complicated, and
// demands more of the student than we expect at this point in the track.

impl<T> Into<Vec<T>> for SimpleLinkedList<T> {
    // Consumes the list
    fn into(mut self) -> Vec<T> {
        let mut v = Vec::new();
        while let Some(h) = self.pop() {
            v.push(h);
        }
        v.reverse();
        v
    }
}
