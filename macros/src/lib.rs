#[macro_export(local_inner_macros)]
macro_rules! hashmap {
    () => { ::std::collections::HashMap::new() };
    ($($key:expr => $value:expr,)+) => { hashmap!($($key => $value),+) };
    ($($key:expr => $value:expr),* ) => {
        {
            let mut map = ::std::collections::HashMap::new();
            $(
                let _ = map.insert($key, $value);
            )*
            map
        }
    };
}
