use std::cmp::Ordering;

#[derive(Debug, PartialEq)]
pub enum Error {
    NotEnoughPinsLeft,
    GameComplete,
}

#[derive(Debug)]
pub enum Frame {
    Strike(),                // 2 next rolls
    Spare(u16),              // first roll of Spare, 1 next roll
    Open(u16, u16),          // the 2 rolls of the frame
    Extra(u16, Option<u16>), // extra shots to compute last spare/strike
}

pub struct BowlingGame {
    frames: Vec<Frame>,
    current: Option<u16>,
}

impl Default for BowlingGame {
    fn default() -> Self {
        BowlingGame {
            frames: Vec::new(),
            current: None,
        }
    }
}

impl BowlingGame {
    pub fn new() -> Self {
        BowlingGame::default()
    }

    pub fn roll(&mut self, pins: u16) -> Result<(), Error> {
        if pins > 10 {
            return Err(Error::NotEnoughPinsLeft);
        }
        if self.game_complete() {
            return Err(Error::GameComplete);
        }
        if self.frames.len() >= 10 {
            // extra shot
            self.extra(pins)?;
        } else {
            match self.current {
                None => self.first(pins)?,
                Some(n) => self.second(n, pins)?,
            }
        }
        Ok(())
    }

    fn game_complete(&self) -> bool {
        if self.frames.len() < 10 {
            false
        } else {
            match self.frames[9] {
                Frame::Strike() => 
                    self.frames.len() == 11
                        && matches!(self.frames[10], Frame::Extra(_, Some(_))),
                
                Frame::Spare(_) => 
                    self.frames.len() == 11
                        && matches!(self.frames[10], Frame::Extra(_, None)),
                
                _ => self.frames.len() == 10,
            }
        }
    }

    fn extra(&mut self, pins: u16) -> Result<(), Error> {
        if self.frames.len() == 10 {
            println!("extra 1");
            self.frames.push(Frame::Extra(pins, None));
        } else {
            println!("extra 2");
            match self.frames.pop().unwrap() {
                Frame::Extra(f, None) => {
                    if f != 10 && f + pins > 10 {
                        return Err(Error::NotEnoughPinsLeft);
                    } else {
                        self.frames.push(Frame::Extra(f, Some(pins)));
                    }
                }
                _ => panic!("Wrong way !"),
            }
        }
        Ok(())
    }

    fn first(&mut self, pins: u16) -> Result<(), Error> {
        if pins == 10 {
            self.frames.push(Frame::Strike())
        } else {
            self.current = Some(pins)
        }
        Ok(())
    }

    fn second(&mut self, first: u16, pins: u16) -> Result<(), Error> {
        match 10_u16.cmp(&(first+pins)) {
            Ordering::Equal => self.frames.push(Frame::Spare(first)),
            Ordering::Greater => self.frames.push(Frame::Open(first, pins)),
            Ordering::Less => return Err(Error::NotEnoughPinsLeft),
        }
        self.current.take();
        Ok(())
    }

    pub fn score(&self) -> Option<u16> {
        println!("Frames: {:?}", self.frames);

        if !self.game_complete() {
            return None;
        }

        // reverse iterate
        let mut score: u16 = 0;
        let mut last: u16 = 0;
        let mut penult: u16 = 0;
        for f in self.frames.iter().rev() {
            match f {
                Frame::Open(f, s) => {
                    println!("open : {} + {}", f, s);
                    score += f + s;
                    last = *f;
                    penult = *s
                }
                Frame::Spare(v) => {
                    println!("spare : 10 + {}",  last);
                    score += 10 + last;
                    last = *v;
                    penult = 10 - v;
                }
                Frame::Strike() => {
                    println!("Strike : 10 + {} + {}",  last, penult);
                    score += 10 + last + penult;
                    penult = last;
                    last = 10;
                }
                Frame::Extra(f, s) => {
                    println!("Extra ");
                    penult = s.unwrap_or(0);
                    last = *f;
                }
            }
        }
        Some(score)
    }
}
