use std::ops::Add;

pub enum Triangle {
    Equilateral,
    Isocele,
    Scalene,
}

fn is_valid<T: PartialEq + PartialOrd + Add<Output = T> + Default + Copy>(
    a: T,
    b: T,
    c: T,
) -> bool {
    // also a=0 <=> a + a = a
    !is_zero(a)
        && b != T::default()
        && c != T::default()
        && a + b >= c
        && a + c >= b
        && b + c >= a
}

fn is_zero<T: PartialEq + Add<Output=T> + Copy>(val: T) -> bool {
    val + val == val
}

impl Triangle {
    pub fn build<T: PartialEq + PartialOrd + Add<Output = T> + Default + Copy>(
        sides: [T; 3],
    ) -> Option<Triangle> {
        let [a, b, c] = sides;
        if !is_valid(a, b, c) {
            return None;
        }
        if a == b && b == c {
            return Some(Triangle::Equilateral);
        }
        if a == b || b == c || a == c {
            return Some(Triangle::Isocele);
        }

        Some(Triangle::Scalene)
    }

    pub fn is_equilateral(&self) -> bool {
        matches!(*self, Self::Equilateral)
    }

    pub fn is_scalene(&self) -> bool {
        matches!(*self, Self::Scalene)
    }

    pub fn is_isosceles(&self) -> bool {
        matches!(*self, Self::Isocele)
    }
}
