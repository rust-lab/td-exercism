pub struct Luhn {
    data: String,
}

pub enum Error {
    InvalidChar(char),
}

impl Luhn {
    pub fn is_valid(&self) -> bool {
        match Self::trim(self.data.as_str()) {
            Ok(i) => {
                if i.len() < 2 {
                    false
                } else {
                    Self::validate(&i)
                }
            }
            Err(_) => false,
        }
    }

    fn trim(code: &str) -> Result<Vec<u32>, Error> {
        let mut v = Vec::new();
        for c in code.chars().filter(|c| c != &' ') {
            match c.to_digit(10) {
                None => return Err(Error::InvalidChar(c)),
                Some(u) => v.insert(0, u),
            }
        }
        Ok(v)
        // https://exercism.io/tracks/rust/exercises/luhn/solutions/b310e5203500486398d482b47561cac1
        // try_fold, map_or..
    }

    fn validate(input: &[u32]) -> bool {
        input
            .iter()
            .enumerate()
            .map(|(i, d)| if i % 2 == 1 { Self::convert(*d) } else { *d })
            .sum::<u32>()
            % 10
            == 0
    }

    fn convert(n: u32) -> u32 {
        let d = n * 2;
        if d > 9 {
            d - 9
        } else {
            d
        }
    }
}

/// Here is the example of how the From trait could be implemented
/// for the &str type. Naturally, you can implement this trait
/// by hand for the every other type presented in the test suite,
/// but your solution will fail if a new type is presented.
/// Perhaps there exists a better solution for this problem?
impl<T: ToString> From<T> for Luhn {
    fn from(input: T) -> Self {
        Luhn {
            data: input.to_string(),
        }
    }
}
