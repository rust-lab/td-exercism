use std::fmt::Debug;

#[derive(Debug)]
pub struct CircularBuffer<T: Debug> {
    capacity: usize,
    data: Vec<Option<T>>,
    read_pos: CircularIndex,
    write_pos: CircularIndex,
}

#[derive(Debug)]
struct CircularIndex {
    capacity: usize,
    idx: usize,
}

impl CircularIndex {
    fn new(capacity: usize) -> Self {
        CircularIndex { capacity, idx: 0 }
    }

    fn next(&mut self) -> usize {
        let curr = self.idx;
        self.idx = (self.idx + 1) % self.capacity;
        curr
    }
}

#[derive(Debug, PartialEq)]
pub enum Error {
    EmptyBuffer,
    FullBuffer,
}

impl<T: Debug> CircularBuffer<T> {
    pub fn new(capacity: usize) -> Self {
        let mut data = Vec::with_capacity(capacity);
        for _ in 0..capacity {
            data.push(None);
        }
        CircularBuffer {
            capacity,
            write_pos: CircularIndex::new(capacity),
            read_pos: CircularIndex::new(capacity),
            data,
        }
    }

    pub fn write(&mut self, element: T) -> Result<(), Error> {
        let w_idx = self.write_pos.next();
        match self.data[w_idx] {
            None => {
                self.data[w_idx] = Some(element);
                println!("After write {:?}", &self);
                Ok(())
            }
            Some(_) => Err(Error::FullBuffer),
        }
    }

    pub fn read(&mut self) -> Result<T, Error> {
        let r_idx = self.read_pos.next();

        let data_ref = self.data[(r_idx)].take();
        self.data[r_idx] = None;
        println!("After read {:?}", &self);

        match data_ref {
            None => Err(Error::EmptyBuffer),
            Some(v) => Ok(v),
        }
    }

    pub fn clear(&mut self) {
        while self.read().is_ok() {}
    }

    pub fn overwrite(&mut self, element: T) {
        let w_idx = self.write_pos.next();
        match self.data[w_idx] {
            None => {
                self.data[w_idx] = Some(element);
            }
            Some(_) => {
                self.data[w_idx] = Some(element);
                self.read_pos.next();
            }
        }
        println!("After write {:?}", &self);
    }
}
