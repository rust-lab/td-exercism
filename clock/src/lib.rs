use std::fmt::{Display, Formatter, Result};

// clock contains hour:minute, with usual bounds for hours and minutes
#[derive(Debug)]
//#[derive(Eq, )]
#[derive(Eq)]
pub struct Clock {
    hours: u8,
    minutes: u8,
}

// derived
//impl Eq for Clock {}

// could be derived also
impl PartialEq for Clock {
    fn eq(&self, other: &Self) -> bool {
        self.minutes == other.minutes && self.hours == other.hours
    }
}

impl Display for Clock {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "{:0>2}:{:0>2}", self.hours, self.minutes)
    }
}

impl Default for Clock {
    fn default() -> Self {
        Clock {
            hours: 0,
            minutes: 0,
        }
    }
}

impl Clock {
    pub fn new(hours: i32, minutes: i32) -> Self {
        Clock::default().add_hours(hours).add_minutes(minutes)
    }

    pub fn add_hours(&self, hours: i32) -> Self {
        self.add_minutes(60 * hours)
    }

    pub fn add_minutes(&self, minutes: i32) -> Self {
        let mut wk_hours = self.hours as i32;
        let mut wk_minutes = self.minutes as i32 + minutes;
        while wk_minutes > 59 {
            wk_minutes = wk_minutes - 60;
            wk_hours = wk_hours + 1;
        }
        while wk_minutes < 0 {
            wk_minutes = wk_minutes + 60;
            wk_hours = wk_hours - 1;
        }
        while wk_hours < 0 {
            wk_hours = wk_hours + 24;
        }
        Clock {
            minutes: wk_minutes as u8,
            hours: (wk_hours % 24) as u8,
        }
    }
}
