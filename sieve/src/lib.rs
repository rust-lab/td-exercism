pub fn primes_up_to(upper_bound: u64) -> Vec<u64> {
    let mut vec: Vec<u64> = (2..=upper_bound).collect();

    let mut primes = Vec::new();

    while  !vec.is_empty() {
        println!("vec is {:?}", vec);
        let p=vec.remove(0);
        primes.push(p);
        vec = apply_sieve(&vec, p);
    }

    primes

}

fn apply_sieve(vec:  &Vec<u64>, prime: u64) -> Vec<u64>{
    let mut mult = prime*2;
    vec.iter().filter(|num| {
        if **num > mult
        {
            mult = mult + prime;
        }
        if **num == mult {
            return false;
        }
        return true;
    }).map(|n| *n).collect()
}

fn _apply_sieve(vec:  &Vec<u64>, prime: u64) -> Vec<u64>{
    let mut mult = prime*2;
    let mut out = Vec::new();
    for num in vec.iter()
    {
        if *num > mult {
            mult = mult+prime;
        }
        if *num != mult {
            out.push(*num);
        }

    }
    out
}
