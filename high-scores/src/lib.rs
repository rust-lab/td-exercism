use std::cmp::min;

#[derive(Debug)]
pub struct HighScores<'a>{
    scores_array: &'a [u32],
}

impl<'a> HighScores<'a> {
    pub fn new(scores: &'a [u32]) -> Self {
        HighScores {
            scores_array: scores,
        }
    }

    pub fn scores(&self) -> &[u32] {
        self.scores_array
    }

    pub fn latest(&self) -> Option<u32> {
        match self.scores_array.len() {
            0 => None, 
            l => Some(self.scores_array[l-1]),
        }
    }

    pub fn personal_best(&self) -> Option<u32> {
        self.scores_array.iter().max().map(|ref_to_u32| *ref_to_u32)
    }

    pub fn personal_top_three(&self) -> Vec<u32> {
        let mut v = self.scores_array.to_vec();
        v.sort_by(|a, b| b.cmp(a));
        v[..(min(v.len(), 3))].to_vec()
    }
}
