use std::collections::HashMap;

pub fn count(nucleotide: char, dna: &str) -> Result<usize, char> {
    validate_nucleotide(&nucleotide)?;

    // find unexpected chars
    if let Some(c) = dna.chars().find(|c| !is_valid_nucleotide(c)) {
        return Err(c);
    }

    Ok(dna.chars().filter(|c| *c == nucleotide).count())
}

pub fn nucleotide_counts(dna: &str) -> Result<HashMap<char, usize>, char> {
    let mut map = HashMap::default();

    for c in "ACGT".chars() {
        map.insert(c, count(c, dna)?);
    }

    Ok(map)
}

fn is_valid_nucleotide(c: &char) -> bool {
    match c {
        'A' | 'C' | 'G' | 'T' => true,
        _ => false,
    }
}

fn validate_nucleotide(c: &char) -> Result<char, char> {
    match c {
        'A' | 'C' | 'G' | 'T' => Ok(*c),
        _ => Err(*c),
    }
}
