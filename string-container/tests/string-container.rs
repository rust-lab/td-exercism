use string_container::Container;

#[test]
fn test_new(){
    let c = Container::new("toto");
    assert_eq!("toto", c.value());
    // c is moved above. Following line will not compile
    //assert_eq!("toto", c.value());

}