use std::marker::PhantomData;

pub struct Container<'a> {
    val : &'a str,

}

impl<'a> Container<'a> {
    pub fn new(value: &'a str) -> Container {
        Container {
            val: value,
        }
    }

    pub fn value(self) -> &'a str {
       self.val
    }
}


