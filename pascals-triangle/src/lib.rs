/**
 1
 1 1
 1 2 1
 1 3 3 1
 1 4 6 4 1



*/

pub struct PascalsTriangle{
    rows: Vec<Vec<u32>>,
}

impl PascalsTriangle {


    pub fn new(row_count: usize) -> Self {
        let mut new = PascalsTriangle{
            rows: Vec::with_capacity(row_count)
        };

        match row_count {
            0 => new,
            n => {
                // init first row
                new.rows.push(vec![1]);

                while(new.rows.len() < n) {
                    new.rows.push(Self::build_next(new.rows.get(new.rows.len()-1).unwrap()));
                }
                new
            }
        }
    }

    pub fn rows(&self) -> Vec<Vec<u32>> {
       self.rows.clone()
    }

    fn build_next(row: &Vec<u32>) -> Vec<u32> {
        let mut res = Vec::with_capacity(row.len()+1);
        res.push(1);
        for i in 1..row.len()+1 {
            let left = row.get(i-1).unwrap_or(&0_u32);
            let right = row.get(i).unwrap_or(&0_u32);
            res.push(left+right);
        }
        res
    }
}
