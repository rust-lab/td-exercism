use std::convert::TryInto;
use std::ops::{Add, Mul, Sub};

// holds the char transcription 'a' => 0, 'b'=>1 ...
pub(crate) struct CharTrans {
    n: i32,
}

impl CharTrans {
    pub fn value(&mut self) -> u8 {
        while self.n < 0 {
            self.n += 26;
        }
        self.n %= 26;
        self.n.try_into().unwrap()
    }
}

impl From<char> for CharTrans {
    fn from(c: char) -> Self {
        CharTrans {
            n: (c as u8 - b'a') as i32,
        }
    }
}

impl From<CharTrans> for char {
    fn from(mut t: CharTrans) -> Self {
        (t.value() + b'a') as char
    }
}

impl Add<i32> for CharTrans {
    type Output = CharTrans;

    fn add(self, rhs: i32) -> Self::Output {
        CharTrans { n: self.n + rhs }
    }
}

impl Sub<i32> for CharTrans {
    type Output = CharTrans;

    fn sub(self, rhs: i32) -> Self::Output {
        CharTrans { n: self.n - rhs }
    }
}
impl Mul<i32> for CharTrans {
    type Output = CharTrans;

    fn mul(self, rhs: i32) -> Self::Output {
        CharTrans { n: self.n * rhs }
    }
}
