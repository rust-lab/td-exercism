mod chars;
mod encoder;

pub use encoder::AffineCipherError;
use encoder::Encoder;

/// Encodes the plaintext using the affine cipher with key (`a`, `b`). Note that, rather than
/// returning a return code, the more common convention in Rust is to return a `Result`.
pub fn encode(plaintext: &str, a: i32, b: i32) -> Result<String, AffineCipherError> {
    let encoder = Encoder::new(a, b)?;

    let mut res = String::new();
    let mut i: usize = 0;
    for e in plaintext.chars() {
        if e.is_alphanumeric() {
            i += 1;
            res.push(encoder.encode(e));
            if i % 5 == 0 {
                res.push(' ');
            }
        }
    }
    Ok(res.trim().to_string())
}

/// Decodes the ciphertext using the affine cipher with key (`a`, `b`). Note that, rather than
/// returning a return code, the more common convention in Rust is to return a `Result`.
pub fn decode(ciphertext: &str, a: i32, b: i32) -> Result<String, AffineCipherError> {
    let encoder = Encoder::new(a, b)?;

    Ok(ciphertext
        .chars()
        .filter(|l| l != &' ')
        .map(|l| encoder.decode(l))
        .collect())
}
