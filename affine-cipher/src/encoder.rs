use crate::chars::CharTrans;

/// While the problem description indicates a return status of 1 should be returned on errors,
/// it is much more common to return a `Result`, so we provide an error type for the result here.
#[derive(Debug, Eq, PartialEq)]
pub enum AffineCipherError {
    NotCoprime(i32),
}

pub struct Encoder {
    a: i32,
    b: i32,
    inv: i32,
}

impl Encoder {
    pub fn new(a: i32, b: i32) -> Result<Self, AffineCipherError> {
        Self::validate_key(a, 26)?;
        Ok(Encoder {
            a,
            b,
            inv: Self::mmi(a, 26),
        })
    }

    fn validate_key(a: i32, m: u32) -> Result<(), AffineCipherError> {
        match Self::euclide(a, m as i32) {
            (1, _, _) => Ok(()),
            _ => Err(AffineCipherError::NotCoprime(a)),
        }
    }

    pub fn encode(&self, mut letter: char) -> char {
        if letter.is_numeric() {
            // dont encode
            letter
        } else {
            letter = letter.to_ascii_lowercase();
            let num = CharTrans::from(letter);
            char::from(num * self.a + self.b)
        }
    }

    pub fn decode(&self, encoded: char) -> char {
        if encoded.is_numeric() {
            // dont decode
            encoded
        } else {
            let trans = CharTrans::from(encoded);
            char::from((trans - self.b) * self.inv)
        }
    }

    fn euclide_algo(r: i32, u: i32, v: i32, r0: i32, u0: i32, v0: i32) -> (i32, i32, i32) {
        match r0 {
            0 => (r, u, v),
            _ => Self::euclide_algo(
                r0,
                u0,
                v0,
                r - (r / r0) * r0,
                u - (r / r0) * u0,
                v - (r / r0) * v0,
            ),
        }
    }

    fn euclide(a: i32, b: i32) -> (i32, i32, i32) {
        let (pgcd, u, v) = Self::euclide_algo(a, 1, 0, b, 0, 1);
        assert_eq!(pgcd, u * a + b * v, "Euclide res");
        (pgcd, u, v)
    }

    fn mmi(x: i32, modulo: i32) -> i32 {
        println!("finding modular inverse of {}, mod {}", x, modulo);
        Self::euclide(x, modulo).1
    }
}

#[test]
fn euclide() {
    assert_eq!((1, -9, 47), Encoder::euclide(120, 23));
    Encoder::euclide(5, 26);
    assert_eq!(4, Encoder::mmi(3, 11));
}

#[test]
fn test_encode() {
    //Encoding test gives ybty with the key a=5 b=7
    let e = Encoder::new(5, 7).unwrap();
    assert_eq!('y', e.encode('t'));
    assert_eq!('b', e.encode('e'));
    assert_eq!('t', e.encode('s'));
}

#[test]
fn test_decode() {
    //Encoding test gives ybty with the key a=5 b=7
    let e = Encoder::new(5, 7).unwrap();
    assert_eq!('t', e.decode('y'));
    assert_eq!('e', e.decode('b'));
    assert_eq!('s', e.decode('t'));
}
