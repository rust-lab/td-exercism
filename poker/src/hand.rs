use super::card::*;

use crate::hand::HandType::{Paire, Undef};
use crate::hand_type::HandType;
use crate::hand_type::HandType::{Brelan, DoublePaire};
use log::*;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::fmt;
use std::fmt::{Debug, Formatter};

pub const POW15_5: u64 = 15 * 15 * 15 * 15 * 15;
const POW15_4: u64 = 15 * 15 * 15 * 15;
const POW15_3: u64 = 15 * 15 * 15;
const POW15_2: u64 = 15 * 15;
const POW15_1: u64 = 15;
const POW15_0: u64 = 1;

pub struct Hand<'a> {
    cards: Vec<Card>,
    hand_type: HandType,
    string: &'a str,
}

impl<'a> Debug for Hand<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.string)
    }
}

impl<'a> Hand<'a> {
    fn get_card_map(&self) -> HashMap<u8, Vec<Card>> {
        let mut map: HashMap<u8, Vec<Card>> = HashMap::new();
        for c in &self.cards {
            match map.get_mut(&c.get_value()) {
                None => {
                    map.insert(c.get_value(), vec![c.clone()]);
                }
                Some(v) => {
                    v.push(c.clone());
                }
            };
        }
        debug!(target:"card_map", "map {:?}", map);
        map
    }

    pub fn find_hand(&mut self) {
        self.cards
            .sort_by(|c1, c2| c2.partial_cmp(c1).unwrap_or(Ordering::Equal));
        let card_map = self.get_card_map();
        if let Some(hand) = self.get_quinte_flush() {
            self.hand_type = hand;
        } else if let Some(hand) = self.get_carre(&card_map) {
            self.hand_type = hand;
        } else if let Some(hand) = self.get_full(&card_map) {
            self.hand_type = hand;
        } else if let Some(hand) = self.get_couleur() {
            self.hand_type = hand;
        } else if let Some(hand) = self.get_quinte() {
            self.hand_type = hand;
        } else if let Some(hand) = self.get_brelan(&card_map) {
            self.hand_type = hand;
        } else if let Some(hand) = self.get_double_paire(&card_map) {
            self.hand_type = hand;
        } else if let Some(hand) = self.get_paire(&card_map) {
            self.hand_type = hand;
        } else {
            let c1 = self.cards.get(0).unwrap().get_value() as u64;
            let c2 = self.cards.get(1).unwrap().get_value() as u64;
            let c3 = self.cards.get(2).unwrap().get_value() as u64;
            let c4 = self.cards.get(3).unwrap().get_value() as u64;
            let c5 = self.cards.get(4).unwrap().get_value() as u64;
            self.hand_type = HandType::None(
                c1 * POW15_4 + c2 * POW15_3 + c3 * POW15_2 + c4 * POW15_1 + c5 * POW15_0,
            );
        }
        info!(target:"find_hand", "{:?}, Hand type : {:?}", self, self.hand_type);
    }

    pub fn consume_str(self) -> &'a str {
        self.string
    }

    pub fn clone_type(&self) -> HandType {
        self.hand_type.clone()
    }

    fn get_quinte_flush(&self) -> Option<HandType> {
        match self.get_couleur() {
            None => None,
            Some(_) => match self.get_quinte() {
                Some(HandType::Quinte(card)) => Some(HandType::QuinteFlush(card)),
                _ => None,
            },
        }
    }

    fn get_carre(&self, card_map: &HashMap<u8, Vec<Card>>) -> Option<HandType> {
        let opt_carre = card_map.iter().map(|(_, v)| v).find(|v| v.len() == 4);
        match opt_carre {
            None => None,
            Some(carre) => {
                let other = self
                    .cards
                    .iter()
                    .filter(|c| !carre.contains(&c))
                    .cloned()
                    .collect::<Vec<Card>>();
                Some(HandType::Carre(
                    carre.get(0).unwrap().get_value() as u64 * POW15_1
                        + other.get(0).unwrap().get_value() as u64,
                ))
            }
        }
    }

    fn get_full(&self, card_map: &HashMap<u8, Vec<Card>>) -> Option<HandType> {
        let opt_brelan = card_map.iter().map(|(_, v)| v).find(|v| v.len() == 3);
        let opt_pair = card_map.iter().map(|(_, v)| v).find(|v| v.len() == 2);
        match (opt_brelan, opt_pair) {
            (Some(brelan), Some(pair)) => Some(HandType::Full(
                brelan.get(0).unwrap().get_value() as u64 * POW15_1
                    + pair.get(0).unwrap().get_value() as u64,
            )),
            _ => None,
        }
    }

    fn get_couleur(&self) -> Option<HandType> {
        let color = self.cards.get(0).unwrap().get_color();
        let mut score: u64 = 0;
        // cards are sorted desc
        for card in self.cards.iter() {
            score *= POW15_1;
            score += card.get_value() as u64;
            if card.get_color() != color {
                return None;
            }
        }
        Some(HandType::Couleur(score))
    }

    fn get_quinte(&self) -> Option<HandType> {
        debug!(target: "get_quinte", "quinte : {:?}", self.cards);
        // check A,2,3,4,5
        if self.cards.get(0).unwrap().get_value() == 14
            && self.cards.get(1).unwrap().get_value() == 5
            && self.cards.get(2).unwrap().get_value() == 4
            && self.cards.get(3).unwrap().get_value() == 3
            && self.cards.get(4).unwrap().get_value() == 2
        {
            return Some(HandType::Quinte(self.cards.get(1).unwrap().clone()));
        }

        let mut last = self.cards.get(0).unwrap().get_value();
        if last < 5 {
            return None;
        }
        for i in 1..5 {
            last -= 1;
            let curr = self.cards.get(i).unwrap().get_value();
            if last != curr {
                debug!(target: "get_quinte", "last {}, curr {}", last, curr);
                return None;
            }
        }
        Some(HandType::Quinte(self.cards.get(0).unwrap().clone()))
    }

    fn get_brelan(&self, card_map: &HashMap<u8, Vec<Card>>) -> Option<HandType> {
        let opt_vec = card_map.iter().map(|(_, v)| v).find(|v| v.len() == 3);
        match opt_vec {
            None => None,
            Some(v) => {
                let others: Vec<Card> = self
                    .cards
                    .iter()
                    .filter(|c| !v.contains(&c))
                    .cloned()
                    .collect();
                let c3: u64 = others.get(1).unwrap().get_value() as u64;
                let c2: u64 = others.get(0).unwrap().get_value() as u64;
                let c1: u64 = v.get(0).unwrap().get_value() as u64;

                Some(Brelan((c1 * 15 + c2) * 15 + c3))
            }
        }
    }

    fn get_double_paire(&self, card_map: &HashMap<u8, Vec<Card>>) -> Option<HandType> {
        let mut vec: Vec<Vec<Card>> = card_map
            .values()
            .filter(|v| v.len() == 2)
            .map(|v| v.to_owned())
            .collect();
        debug!(target:"eval_type", "get double paire : vec {:?}", vec);
        if vec.len() == 2 {
            vec.sort_by(|c1, c2| c1.partial_cmp(c2).unwrap_or(Ordering::Equal));
            let second = vec.get(0).unwrap().to_owned();
            let first = vec.get(1).unwrap().to_owned();
            let other = self
                .cards
                .iter()
                .filter(|c| !(second.contains(&c) || first.contains(&c)))
                .cloned()
                .next();

            let c1 = first.get(0).unwrap().get_value() as u64;
            let c2 = second.get(0).unwrap().get_value() as u64;
            let c3 = other.unwrap().get_value() as u64;
            Some(DoublePaire((c1 * 15 + c2) * 15 + c3))
        } else {
            None
        }
    }

    fn get_paire(&self, card_map: &HashMap<u8, Vec<Card>>) -> Option<HandType> {
        let opt_vec = card_map.iter().map(|(_, v)| v).find(|v| v.len() == 2);
        match opt_vec {
            None => None,
            Some(v) => {
                let others: Vec<Card> = self
                    .cards
                    .iter()
                    .filter(|c| !v.contains(&c))
                    .cloned()
                    .collect();

                let c1: u64 = v.get(0).unwrap().get_value() as u64;
                let c2: u64 = others.get(0).unwrap().get_value() as u64;
                let c3: u64 = others.get(1).unwrap().get_value() as u64;
                let c4: u64 = others.get(2).unwrap().get_value() as u64;
                Some(Paire(((c1 * 15 + c2) * 15 + c3) * 15 + c4))
            }
        }
    }
}

impl<'a> PartialEq for Hand<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.string == other.string
    }
}

impl<'a> TryFrom<&'a str> for Hand<'a> {
    type Error = Errors;

    fn try_from(value: &'a str) -> Result<Self, Self::Error> {
        let mut cards = vec![];
        let string = value.to_string();

        for str_card in string.split(' ') {
            cards.push(Card::try_from(str_card.trim())?)
        }
        Ok(Hand::<'a> {
            hand_type: Undef,
            cards,
            string: value,
        })
    }
}

impl<'a> From<Hand<'a>> for &'a str {
    fn from(h: Hand<'a>) -> Self {
        h.consume_str()
    }
}

impl<'a> PartialOrd<Hand<'a>> for Hand<'a> {
    fn partial_cmp(&self, other: &Hand<'a>) -> Option<Ordering> {
        debug!(target:"hand_cmp", "-- comparing {:?} and {:?}", self, other);
        if self.hand_type == Undef || other.hand_type == Undef {
            debug!(target:"hand_cmp", "-- comparing {:?} and {:?} : None", self, other);
            None
        } else {
            self.hand_type.partial_cmp(&other.hand_type)
        }
    }
}

impl<'a> PartialEq<HandType> for Hand<'a> {
    fn eq(&self, other: &HandType) -> bool {
        self.hand_type == *other
    }
}

impl<'a> PartialOrd<HandType> for Hand<'a> {
    fn partial_cmp(&self, other: &HandType) -> Option<Ordering> {
        debug!(target:"hand_hand_type_cmp",
               "--- Comparing hand and handType : {:?} and {:?}",
            self, other
        );
        if self.hand_type == Undef || *other == Undef {
            None
        } else {
            self.hand_type.partial_cmp(other)
        }
    }
}
