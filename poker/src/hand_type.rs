use super::hand::POW15_5;
use crate::card::Card;
use log::*;
use std::cmp::Ordering;

#[derive(Debug, PartialEq, Clone)]
pub enum HandType {
    None(u64),
    Undef,
    Paire(u64),
    DoublePaire(u64),
    Brelan(u64),
    Quinte(Card),
    Couleur(u64),
    Full(u64),
    Carre(u64),
    QuinteFlush(Card),
}

impl HandType {
    fn get_rank(&self) -> u64 {
        match self {
            HandType::Undef => panic!("Whoo"),
            HandType::None(s) => POW15_5 + s,
            HandType::Paire(s) => 2 * POW15_5 + s,
            HandType::DoublePaire(s) => 3 * POW15_5 + s,
            HandType::Brelan(s) => 4 * POW15_5 + s,
            HandType::Quinte(c) => 5 * POW15_5 + c.get_value() as u64,
            HandType::Couleur(s) => 6 * POW15_5 + s,
            HandType::Full(s) => 7 * POW15_5 + s,
            HandType::Carre(s) => 8 * POW15_5 + s,
            HandType::QuinteFlush(c) => 9 * POW15_5 + c.get_value() as u64,
        }
    }
}

impl PartialOrd<HandType> for HandType {
    fn partial_cmp(&self, other: &HandType) -> Option<Ordering> {
        debug!(target:"hand_type_cmp", "--- Comparing hands by type : {:?} and {:?}", self, other);
        let order = self.get_rank().cmp(&other.get_rank());
        Some(order)
    }
}
