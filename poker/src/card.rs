use std::cmp::Ordering;
use std::convert::TryFrom;
use std::fmt::{Debug, Formatter};
use std::ops::Add;

#[derive(Debug, PartialEq)]
pub enum Errors {
    CardBadLength(String),
    CardBadValue(String),
    CardBadColor(String),
}

#[derive(PartialEq, Eq, Clone)]
pub enum Color {
    PIQUE,
    CARREAU,
    COEUR,
    TREFLE,
}

#[derive(PartialEq, Eq, Clone)]
pub struct Value {
    pub val: u8,
}

#[derive(Clone)]
pub struct Card {
    value: Value,
    color: Color,
}

impl Debug for Color {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Color::PIQUE => write!(f, "S"),
            Color::CARREAU => write!(f, "D"),
            Color::COEUR => write!(f, "H"),
            Color::TREFLE => write!(f, "C"),
        }
    }
}

impl Debug for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.val)
    }
}

impl Debug for Card {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Card:{:?}{:?}", self.value, self.color)
    }
}

impl From<&Color> for String {
    fn from(color: &Color) -> Self {
        match color {
            Color::PIQUE => "S",
            Color::CARREAU => "D",
            Color::COEUR => "H",
            Color::TREFLE => "C",
        }
        .to_string()
    }
}

impl Ord for Value {
    fn cmp(&self, right: &Self) -> Ordering {
        match (self.val, &right.val) {
            (1, 1) => Ordering::Equal,
            (1, _) => Ordering::Greater,
            (_, 1) => Ordering::Less,
            (l, r) => l.cmp(r),
        }
    }
}

impl PartialOrd<Value> for Value {
    fn partial_cmp(&self, right: &Value) -> Option<Ordering> {
        Some(self.cmp(right))
    }
}

impl From<&Value> for String {
    fn from(value: &Value) -> Self {
        match value.val {
            1 => "A",
            2 => "2",
            3 => "3",
            4 => "4",
            5 => "5",
            6 => "6",
            7 => "7",
            8 => "8",
            9 => "9",
            10 => "10",
            11 => "J",
            12 => "Q",
            13 => "K",
            _ => unreachable!(),
        }
        .to_string()
    }
}

impl Card {
    pub fn new(val: u8, color: Color) -> Self {
        if val < 1 || val > 13 {
            panic!("Not a card value");
        }
        Card {
            value: Value { val },
            color,
        }
    }

    pub fn get_value(&self) -> u8 {
        if self.value.val != 1 {
            self.value.val
        } else {
            14
        }
    }

    pub fn get_color(&self) -> Color {
        self.color.clone()
    }
}

impl PartialEq<Card> for Card {
    fn eq(&self, other: &Self) -> bool {
        self.value == other.value
    }
}

impl PartialOrd<Card> for Card {
    fn partial_cmp(&self, right: &Card) -> Option<Ordering> {
        Some(self.value.cmp(&right.value))
    }
}

impl TryFrom<&str> for Card {
    type Error = Errors;
    fn try_from(s: &str) -> Result<Self, Self::Error> {
        if s.len() != 2 && s.len() != 3 {
            return Err(Errors::CardBadLength(s.to_string()));
        }
        let value = match &s[0..s.len() - 1] {
            "1" | "A" | "a" => Value { val: 1 },
            "2" => Value { val: 2 },
            "3" => Value { val: 3 },
            "4" => Value { val: 4 },
            "5" => Value { val: 5 },
            "6" => Value { val: 6 },
            "7" => Value { val: 7 },
            "8" => Value { val: 8 },
            "9" => Value { val: 9 },
            "10" => Value { val: 10 },
            "J" | "j" => Value { val: 11 },
            "Q" | "q" => Value { val: 12 },
            "K" | "k" => Value { val: 13 },
            _ => return Err(Errors::CardBadValue(s.to_string())),
        };

        let color = match s.chars().nth(s.len() - 1) {
            Some('S') => Color::PIQUE,
            Some('H') => Color::COEUR,
            Some('D') => Color::CARREAU,
            Some('C') => Color::TREFLE,
            _ => return Err(Errors::CardBadColor(s.to_string())),
        };
        Ok(Card { value, color })
    }
}

impl From<&Card> for String {
    fn from(card: &Card) -> Self {
        String::from(&card.value).add(String::from(&card.color).as_str())
    }
}

/***************************/
/*          tests          */
/***************************/

#[test]
fn test_cards_try_from_ok() {
    assert_eq!(
        Card::try_from("4H"),
        Ok(Card {
            value: Value { val: 4 },
            color: Color::COEUR
        })
    );
    assert_eq!(Card::try_from("aD"), Ok(Card::new(1, Color::CARREAU)));
    assert_eq!(Card::try_from("1S"), Ok(Card::new(1, Color::PIQUE)));
    assert_eq!(Card::try_from("10C"), Ok(Card::new(10, Color::TREFLE)));
    assert_eq!(Card::try_from("QS"), Ok(Card::new(12, Color::PIQUE)));
    assert_eq!(Card::try_from("jH"), Ok(Card::new(11, Color::COEUR)));
    assert_eq!(Card::try_from("KD"), Ok(Card::new(13, Color::CARREAU)));
}

#[test]
fn test_cards_try_from_err() {
    assert_eq!(
        Card::try_from("11D"),
        Err(Errors::CardBadValue("11D".to_string()))
    );
    assert_eq!(
        Card::try_from(""),
        Err(Errors::CardBadLength("".to_string()))
    );
    assert_eq!(
        Card::try_from("11Ds"),
        Err(Errors::CardBadLength("11Ds".to_string()))
    );
    assert_eq!(
        Card::try_from("10Q"),
        Err(Errors::CardBadColor("10Q".to_string()))
    );
}

#[test]
fn test_value_cmp() {
    assert!(Value { val: 1 } > Value { val: 8 });
    assert!(Value { val: 9 } > Value { val: 8 });
    assert!(Value { val: 9 } < Value { val: 1 });
}

#[test]
fn test_card_cmp() {
    assert!(Card::new(9, Color::CARREAU) > Card::new(8, Color::CARREAU));
    assert!(Card::new(1, Color::CARREAU) > Card::new(8, Color::CARREAU));
    assert_eq!(Card::new(8, Color::CARREAU), Card::new(8, Color::CARREAU));
    assert_eq!(Card::new(8, Color::CARREAU), Card::new(8, Color::PIQUE));
}

#[test]
fn test_to_string() {
    assert_eq!(
        String::from(&Card::new(8, Color::CARREAU)),
        String::from("8D")
    );
    assert_eq!(
        String::from(&Card::new(1, Color::PIQUE)),
        String::from("AS")
    );
    assert_eq!(
        String::from(&Card::new(11, Color::TREFLE)),
        String::from("JC")
    );
    assert_eq!(
        String::from(&Card::new(12, Color::COEUR)),
        String::from("QH")
    );
    assert_eq!(
        String::from(&Card::new(13, Color::TREFLE)),
        String::from("KC")
    );
    assert_eq!(
        String::from(&Card::new(10, Color::CARREAU)),
        String::from("10D")
    );
}
