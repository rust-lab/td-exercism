/// Given a list of poker hands, return a list of those hands which win.
///
/// Note the type signature: this function should return _the same_ reference to
/// the winning hand(s) as were passed in, not reconstructed strings which happen to be equal.
mod card;
mod hand;
mod hand_type;

use hand::Hand;
use log::*;
use simplelog::{Config, SimpleLogger};
use std::cmp::Ordering;
use std::convert::TryFrom;

static LEVEL: LevelFilter = LevelFilter::Info;

pub fn winning_hands<'a>(hands: &[&'a str]) -> Option<Vec<&'a str>> {
    SimpleLogger::init(LEVEL, Config::default()).ok();
    let mut vec_hands: Vec<Hand<'a>> = hands
        .iter()
        .map(|s| Hand::try_from(*s).unwrap()) // converts str to hand
        .map(|mut h| {
            h.find_hand();
            h
        }) // apply find_hands
        .collect();
    vec_hands.sort_by(|h1, h2| h1.partial_cmp(h2).unwrap_or(Ordering::Equal));
    info!(target:"winning_hands", "sorted vec : {:?}", vec_hands);

    // note : into_iter takes ownership of vec's elements
    // note 2 : h.into() converts hand to &str, using 'impl From<Hand> for &str' implementation
    Some(
        find_max(vec_hands)
            .into_iter()
            .map(|h| (h).into())
            .collect(),
    )
}

fn find_max(mut vec: Vec<Hand>) -> Vec<Hand> {
    // vec must be ordered, vec must not be empty
    vec.reverse();
    let max = vec.get(0).unwrap().clone_type();
    info!(target:"find_max","max : {:?}", max);

    vec.into_iter()
        .filter(|hand| hand.partial_cmp(&max).unwrap_or(Ordering::Equal) == Ordering::Equal)
        .collect()
}
