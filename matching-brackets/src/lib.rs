pub fn brackets_are_balanced(string: &str) -> bool {
    stack_matcher(string)
    //recursive_matcher(&string.as_bytes(), 0, 0, 0)
}

// not working on "[({]})"
fn recursive_matcher(string: &[u8], par: u8, curl: u8, brack: u8) -> bool {
    println!(
        "call len:{}, balance : {}, {}, {}",
        string.len(),
        par,
        curl,
        brack
    );
    match string.len() {
        0 => par == 0 && curl == 0 && brack == 0,
        _ => {
            let head = string[0_usize] as char;
            let tail = &string[1_usize..];
            match head {
                '{' => {
                    println!("opening {{");
                    recursive_matcher(tail, par, curl + 1, brack)
                }
                '(' => {
                    println!("opening (");
                    recursive_matcher(tail, par + 1, curl, brack)
                }
                '[' => {
                    println!("opening [");
                    recursive_matcher(tail, par, curl, brack + 1)
                }
                '}' => {
                    println!("closing }}");
                    curl > 0 && recursive_matcher(tail, par, curl - 1, brack)
                }
                ')' => {
                    println!("closing )");
                    par > 0 && recursive_matcher(tail, par - 1, curl, brack)
                }
                ']' => {
                    println!("closing ]");
                    brack > 0 && recursive_matcher(tail, par, curl, brack - 1)
                }
                _ => recursive_matcher(tail, par, curl, brack),
            }
        }
    }
}

fn stack_matcher(string: &str) -> bool {
    let mut stack = Vec::new();
    for c in string.chars() {
        match c {
            '[' | '{' | '(' => stack.push(c),
            ']' => {
                if Some('[') != stack.pop() {
                    return false;
                }
            }

            '}' => {
                if '{' != stack.pop().unwrap_or('_') {
                    return false;
                }
            }

            ')' => {
                if '(' != stack.pop().unwrap_or('_') {
                    return false;
                }
            }
            _ => {}
        }
    }
    return stack.is_empty();
}
