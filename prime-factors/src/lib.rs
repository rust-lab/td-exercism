pub fn factors(n: u64) -> Vec<u64> {
    if n == 1 {
        return vec![];
    }
    let mut v_result: Vec<u64> = vec![n];
    loop {
        let m = v_result.pop().unwrap();
        match factorize(m) {
            Some((a, b)) => {
                v_result.push(a);
                v_result.push(b);
            }
            None => {
                v_result.push(m);
                break;
            }
        }
    }

    v_result
}

fn is_factor(n: u64, q: u64) -> bool {
    n % q == 0
}

fn factorize(n: u64) -> Option<(u64, u64)> {
    for i in (2 as u64)..n {
        if is_factor(n, i) {
            return Some((i, n / i));
        }
        if i * i > n {
            return None;
        }
    }
    None
}
