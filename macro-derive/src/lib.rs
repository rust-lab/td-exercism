extern crate proc_macro;

use crate::proc_macro::TokenStream;
use quote::quote;
use syn;
use syn::Data;

use syn::Data::{Struct, Enum, Union};
use syn::Fields::Named;


#[proc_macro_derive(Attributes)]
pub fn with_attrs_derive(input: TokenStream) -> TokenStream {
    let ast: syn::DeriveInput = syn::parse(input).unwrap();
    let name = ast.ident;

    let data = ast.data;
    print_data(data);

    let gen = quote! {
        impl Attributes for #name {
            fn with_attrs(mut self, attrs: &[(&str, &str)]) -> Self {
                for (k, v) in attrs {
                    self.attrs.insert(k.to_string(), v.to_string());
                }
                self
            }

            fn get_attr(&self, key: &str) -> Option<&str> {
                if let Some(v) = self.attrs.get(key) {
                    return Some(v.as_str());
                }
                None
            }
        }
    };
    gen.into()
}

fn print_data(data: Data){
    match data {
        Struct(st_data) => {
            println!("struct");

                match st_data.fields {
                    Named(named) => { println!("named field");
                        for n in named.named.iter() {
                           // println!("name, type : {}, {}", n.ident, n.ty);
                        }
                    },
                    _ => println!("other"),

            }
        },
        Enum(e_data) => println!("enum"),
        Union(u_data) => println!("union"),
    }
}