#[derive(Debug, PartialEq)]
pub enum Error {
    InvalidInputBase,
    InvalidOutputBase,
    InvalidDigit(u32),
}

///
/// Convert a number between two bases.
///
/// A number is any slice of digits.
/// A digit is any unsigned integer (e.g. u8, u16, u32, u64, or usize).
/// Bases are specified as unsigned integers.
///
/// Return an `Err(.)` if the conversion is impossible.
/// The tests do not test for specific values inside the `Err(.)`.
///
///
/// You are allowed to change the function signature as long as all test still pass.
///
///
/// Example:
/// Input
///   number: &[4, 2]
///   from_base: 10
///   to_base: 2
/// Result
///   Ok(vec![1, 0, 1, 0, 1, 0])
///
/// The example corresponds to converting the number 42 from decimal
/// which is equivalent to 101010 in binary.
///
///
/// Notes:
///  * The empty slice ( "[]" ) is equal to the number 0.
///  * Never output leading 0 digits, unless the input number is 0, in which the output must be `[0]`.
///    However, your function must be able to process input with leading 0 digits.
///
pub fn convert(number: &[u32], from_base: u32, to_base: u32) -> Result<Vec<u32>, Error> {
    if from_base < 2 {
        return Err(Error::InvalidInputBase);
    }
    if to_base < 2 {
        return Err(Error::InvalidOutputBase);
    }

    if number.is_empty() {
        return Ok(vec![0]);
    }

    let n = from(number, from_base)?;

    to(n, to_base)
}

fn from(number: &[u32], base: u32) -> Result<u32, Error> {
    let mut pow: u32 = number.len() as u32;

    let mut num = 0_u32;

    for i in number {
        // no need to power, just n=((((i4 * b ) + i3 ) * b + i2 ) * b + i1 ) * b + i0
        pow -= 1;
        if i < &0 || i >= &base {
            return Err(Error::InvalidDigit(*i));
        }
        num += (*i) * base.pow(pow);
    }
    Ok(num)
}

fn to(mut n: u32, base: u32) -> Result<Vec<u32>, Error> {
    let mut vec = Vec::default();
    while n > 0 {
        let d = n % base;
        vec.push(d);
        n = (n - d) / base;
    }
    if vec.is_empty() {
        vec = vec![0];
    }
    Ok(vec.iter().rev().copied().collect())
    // better use insert(0) on the vec
}
