use std::collections::BTreeMap;

pub fn transform(h: &BTreeMap<i32, Vec<char>>) -> BTreeMap<char, i32> {
    let mut new_tree = BTreeMap::default();
    for (point, v_letters) in h.iter() {
        for lettre in v_letters {
            new_tree.insert(uncap(*lettre), *point);
        }
    }
    new_tree
}

fn uncap(l: char) -> char {
    l.to_lowercase().next().unwrap()
}
