/*
99 bottles of beer on the wall, 99 bottles of beer.
Take one down and pass it around, 98 bottles of beer on the wall.
*/

pub fn verse(n: u32) -> String {
    match n {
        0 => "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n".to_owned(),
        1 => "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n".to_owned(),
        b => format!("{b} bottles of beer on the wall, {b} bottles of beer.\nTake one down and pass it around, {r} bottle{s} of beer on the wall.\n", b=b, r=b-1, s=  if b>2 {"s"} else {""}),
    }
}

pub fn sing(start: u32, end: u32) -> String {
    (end..start + 1)
        .map(verse)
        .rev()
        .collect::<Vec<_>>()
        .join("\n")
}
