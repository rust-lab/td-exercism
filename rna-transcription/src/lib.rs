use std::convert::TryFrom;

#[derive(Debug, PartialEq)]
pub struct DNA {
    chain: Vec<DnaNucl>,
}

#[derive(Debug, PartialEq)]
pub struct RNA {
    chain: Vec<RnaNucl>,
}

#[derive(Debug, PartialEq)]
enum RnaNucl {
    A,
    C,
    G,
    U,
}

#[derive(Debug, PartialEq)]
enum DnaNucl {
    A,
    C,
    G,
    T,
}

impl TryFrom<char> for DnaNucl {
    type Error = char;

    fn try_from(car: char) -> Result<Self, Self::Error> {
        match car {
            'A' | 'a' => Ok(DnaNucl::A),
            'C' | 'c' => Ok(DnaNucl::C),
            'G' | 'g' => Ok(DnaNucl::G),
            'T' | 't' => Ok(DnaNucl::T),
            _ => Err(car),
        }
    }
}

impl TryFrom<char> for RnaNucl {
    type Error = char;

    fn try_from(car: char) -> Result<Self, Self::Error> {
        match car {
            'A' | 'a' => Ok(RnaNucl::A),
            'C' | 'c' => Ok(RnaNucl::C),
            'G' | 'g' => Ok(RnaNucl::G),
            'U' | 'u' => Ok(RnaNucl::U),
            _ => Err(car),
        }
    }
}

impl TryFrom<&str> for DNA {
    type Error = usize;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut dna = DNA {
            chain: Vec::default(),
        };
        for (i, n) in value.char_indices() {
            match DnaNucl::try_from(n) {
                Ok(nucl) => dna.chain.push(nucl),
                Err(_) => return Err(i),
            }
        }
        Ok(dna)
    }
}

impl TryFrom<&str> for RNA {
    type Error = usize;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut rna = RNA {
            chain: Vec::default(),
        };
        for (i, n) in value.char_indices() {
            match RnaNucl::try_from(n) {
                Ok(nucl) => rna.chain.push(nucl),
                Err(_) => return Err(i),
            }
        }
        Ok(rna)
    }
}

impl From<DnaNucl> for RnaNucl {
    fn from(d: DnaNucl) -> Self {
        match d {
            DnaNucl::C => RnaNucl::G,
            DnaNucl::G => RnaNucl::C,
            DnaNucl::A => RnaNucl::U,
            DnaNucl::T => RnaNucl::A,
        }
    }
}

impl From<DNA> for RNA {
    fn from(dna: DNA) -> Self {
        let mut chain = Vec::default();
        for n in dna.chain {
            chain.push(RnaNucl::from(n))
        }
        RNA { chain }
    }
}

impl DNA {
    pub fn new(dna: &str) -> Result<DNA, usize> {
        DNA::try_from(dna)
    }

    pub fn into_rna(self) -> RNA {
        RNA::from(self)
    }
}

impl RNA {
    pub fn new(rna: &str) -> Result<RNA, usize> {
        RNA::try_from(rna)
    }
}
