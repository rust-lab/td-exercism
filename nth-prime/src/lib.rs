fn is_factor(n: u32, q: u32) -> bool {
    n % q == 0
}

fn is_prime(n: u32) -> bool {
    for i in 2..n {
        if is_factor(n, i) {
            return false;
        }
        if i * i > n {
            // computing only to sqrt(n)
            return true;
        }
    }
    return true;
}

pub fn nth(n: u32) -> u32 {
    let mut v = Vec::new();
    let mut i: u32 = 2;
    while v.len() <= (n as usize) {
        if is_prime(i) {
            v.push(i);
        }
        i = i + 1;
    }
    return v[n as usize];
}

// smarter : (1..).filter(|c| is_prime(*c)).nth(n as usize)
// a generator, filtering, and picking the n-th element
// https://exercism.io/tracks/rust/exercises/nth-prime/solutions/65a44ef3d18a4e7e9fa85605eecb3fc8
