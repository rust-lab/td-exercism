fn is_factor(n: u32, q: u32) -> bool {
    match q {
        0 => n == 0,
        q => n % q == 0,
    }
}

fn is_factor_of_any(n: u32, factors: &[u32]) -> bool {
    factors
        .to_vec()
        .into_iter()
        .fold(false, |acc, i| acc || is_factor(n, i))
}

pub fn sum_of_multiples(limit: u32, factors: &[u32]) -> u32 {
    (0..limit).filter(|n| is_factor_of_any(*n, factors)).sum()
}
